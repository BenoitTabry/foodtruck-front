export class Catalogue {
    id_produit: number;
    libelle_produit_commerciale: string;
    libelle_produit_technique: string;
    prix: number;
    description: string;
    dispo: string;
    imageUrl: string;
    quantite: number;
    stock: number;
    composition: string;
    nbrVente: number;
}
