export class Actualites {
    id: number;
    titre: string;
    description: string;
    imageUrl: string;
    debut_valid: string;
    fin_valid: string;
    execution: string;
    creation: string;
}
