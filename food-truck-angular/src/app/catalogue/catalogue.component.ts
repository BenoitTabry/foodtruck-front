import { Component, OnInit } from '@angular/core';
import { CatalogueService } from '../services/catalogue.service';
import { Catalogue } from '../models/catalogue';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  products: Catalogue;

  constructor(private cat: CatalogueService) { }

  ngOnInit(): void {
    this.cat.getCat().subscribe(data => {
      this.products = data;
    })
  }

}
