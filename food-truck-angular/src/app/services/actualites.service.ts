import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actualites } from '../models/actualites';

@Injectable({
  providedIn: 'root'
})
export class ActualitesService {
  
  apiUrl = 'http://localhost:8080/actualites';

  constructor(private http: HttpClient) { }

  getActus() {
    return this.http.get<Actualites>(`${this.apiUrl}`);
  }
}
