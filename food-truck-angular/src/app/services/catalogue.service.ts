import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Catalogue } from '../models/catalogue';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  
  apiUrl = 'http://localhost:8081/catalogue/listeCatalogue';

  constructor(private http: HttpClient) { }

  getCat() {
    return this.http.get<Catalogue>(`${this.apiUrl}`);
  }
}
