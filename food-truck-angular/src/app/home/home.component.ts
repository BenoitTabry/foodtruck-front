import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Actualites } from '../models/actualites';
import { ActualitesService } from '../services/actualites.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  id: string;
  actualites: Actualites;

  constructor(private router: Router, private authService: AuthService, private actu: ActualitesService) {
    this.id = localStorage.getItem('token');
  }

  ngOnInit(): void {
    this.actu.getActus().subscribe(data => {
      this.actualites = data;
    })
  }

  logout() {
    console.log('logout');
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
